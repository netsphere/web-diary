
# Q's Web Diary

See https://www.nslabs.jp/webdiary.rhtml



## How to run

`postgres` ユーザになって,
```shell
$ createdb --owner rails --encoding UTF-8 web_diary_development
```

一般ユーザで,
```shell
$ psql -h localhost -U rails web_diary_development
ユーザー rails のパスワード: 

web_diary_development=> \i SETUP_SCHEMA.pgsql 
CREATE TABLE
```

さらに migration
```shell
$ bin/rails db:migrate
```




This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
