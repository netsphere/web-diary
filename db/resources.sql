
-- 〜について
CREATE TABLE resources (
  id         serial PRIMARY KEY,

  -- 1 = url, 2 = asin
  uri_type   int NOT NULL,
  uri        VARCHAR(200) NOT NULL,

  -- 書名.
  title            VARCHAR(200) NOT NULL,

  ------------------------------------
  -- book (手入力)

  -- 版
  edition          VARCHAR(20),
  -- 著者
  authors           VARCHAR(200) ARRAY,
  -- 装丁
  book_designer    VARCHAR(200),
  -- 出版社
  publisher        VARCHAR(200),
  -- 発行日 (出版年)
  -- '2017-01' だったり '2017' のときも.
  publication_date VARCHAR(12),
  -- ジャンル
  genre            VARCHAR(200),
  -- 国
  country          CHAR(2),

  ------------------------------------
  -- 永続データ. <item>要素
  amazon_response_item  xml,
  
  created_at TIMESTAMP NOT NULL,

  UNIQUE (uri_type, uri)
);
