
-- コメント
CREATE TABLE comments (
  id             serial PRIMARY KEY,
  entry_id       int NOT NULL REFERENCES entries (id),

  body           TEXT NOT NULL,

  created_at     TIMESTAMP NOT NULL,
  create_user_id int NOT NULL REFERENCES users (id)
);
