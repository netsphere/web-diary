-- -*- coding:utf-8 -*-

-- 添付ファイル
-- エントリに複数付けられる
CREATE TABLE attach_files (
  id                serial PRIMARY KEY,
  entry_id          int NOT NULL REFERENCES entries (id),

  original_filename VARCHAR(250) NOT NULL,
  content_type      VARCHAR(60) NOT NULL,
  body              BYTEA NOT NULL,
  hashed_filename   VARCHAR(64) NOT NULL UNIQUE,

  created_at        TIMESTAMP NOT NULL,
  create_user_id    int NOT NULL REFERENCES users (id)
);

