
-- ブログ。
-- ユーザグループは複数のブログを持てる
CREATE TABLE blogs (
  id            serial PRIMARY KEY,

  -- 親
  -- ユーザグループが所有する。
  user_group_id int NOT NULL REFERENCES user_groups (id),

  -- あとで migration でこのフィールド追加
  --urlkey        VARCHAR(30) NOT NULL UNIQUE,

  -- 長い名前(表示名)
  name          VARCHAR(100) NOT NULL,

  -- リード文
  lead          VARCHAR(250) NOT NULL,

  -- public_flag   int NOT NULL,
  -- admin_only    int NOT NULL,  -- 管理者だけが更新できる

  theme_filename VARCHAR(250) NOT NULL,

  created_at    TIMESTAMP NOT NULL,
  updated_at    TIMESTAMP
  -- UNIQUE (user_group_id, urlkey)
);

