
-- タグ。エントリに自由に付けられる
-- タグそのものは, グローバルに持つ
CREATE TABLE tags (
  id             serial PRIMARY KEY,
  name           VARCHAR(100) NOT NULL UNIQUE,

  created_at     TIMESTAMP NOT NULL,
  create_user_id int NOT NULL REFERENCES users (id)
);
