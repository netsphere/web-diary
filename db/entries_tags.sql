-- -*- coding:utf-8 -*-

-- エントリとタグの多対多リレーションシップ
CREATE TABLE entries_tags (
  id       serial PRIMARY KEY,
  entry_id int NOT NULL REFERENCES entries (id),
  tag_id   int NOT NULL REFERENCES tags (id),

  UNIQUE (entry_id, tag_id)
);

