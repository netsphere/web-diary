
-- エントリ(記事) と対象
CREATE TABLE entries_resources (
  id          serial PRIMARY KEY,
  entry_id    int NOT NULL REFERENCES entries (id),
  resource_id int NOT NULL REFERENCES resources (id),

  UNIQUE (entry_id, resource_id)
);
