
CREATE TABLE schema_migrations (
  "version"  VARCHAR(255) NOT NULL UNIQUE
);
INSERT INTO schema_migrations (version) VALUES ('20140526002708');
INSERT INTO schema_migrations (version) VALUES ('20170801123757');
INSERT INTO schema_migrations (version) VALUES ('20170806111149'); -- resources_add_fields.rb
INSERT INTO schema_migrations (version) VALUES ('20170807101439'); -- rename_entries_resources.rb
-- 20170914115516_blogs_add_urlkey.rb
INSERT INTO schema_migrations (version) VALUES ('20170917025622'); -- table_amazon_item_cache.rb
