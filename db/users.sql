-- -*- coding:utf-8 -*-

-- ユーザ  
CREATE TABLE users (
  id               serial PRIMARY KEY,
  login            VARCHAR(40) NOT NULL UNIQUE,   -- ログインID
  -- email            VARCHAR(40) NOT NULL UNIQUE, -- メールアドレス

  -- digestは、SHA256で作る
  -- crypted_password VARCHAR(64) NOT NULL, -- 暗号化したパスワード

  -- 氏名(表示名)  foaf:name testing
  name             VARCHAR(40) NOT NULL, 

  -- email_public     int NOT NULL,  -- 非0でメールアドレス公開
  -- expire           TIMESTAMP,            -- 有効期限

  -- FOAFプロパティ
  -- homepage_url     VARCHAR(100) UNIQUE, -- 個人のホームページ。 foaf:homepage stable
  -- homepage_name    VARCHAR(100),
  -- nick             VARCHAR(40), -- foaf:nick  testing
  -- depiction_url    VARCHAR(100), -- 写真やイラスト。 foaf:depiction testing
  -- motto            VARCHAR(200), -- foaf:plan  testing
  -- mob_mail         VARCHAR(40) UNIQUE

  created_at       TIMESTAMP NOT NULL,
  updated_at       TIMESTAMP
);
