

class EntriesSetUpdatedAtNotNull < ActiveRecord::Migration[7.1]
  def self.up
    execute <<EOF
ALTER TABLE entries ALTER COLUMN updated_at SET NOT NULL
EOF
  end
end
