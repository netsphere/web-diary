# -*- coding:utf-8 -*-

# テーブル名の変更
class RenameEntriesResources < ActiveRecord::Migration[7.1]
  def self.up
    execute <<EOF
ALTER TABLE entries_abouts RENAME TO entries_resources;
ALTER TABLE entries_resources RENAME about_id TO resource_id;
EOF
  end
end
