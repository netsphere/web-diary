

require 'digest'

class BlogsAddUrlkey < ActiveRecord::Migration[7.1]
  def self.up
    execute <<EOF
ALTER TABLE blogs ADD COLUMN urlkey VARCHAR(30) UNIQUE
EOF
    Blog.all.each do |blog|
      blog.urlkey = Digest::SHA256.hexdigest(blog.name + blog.id.to_s)[0..9]
      blog.save!
    end
    execute <<EOF
ALTER TABLE blogs ALTER COLUMN urlkey SET NOT NULL
EOF
  end
end
