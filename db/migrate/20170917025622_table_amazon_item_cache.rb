
class TableAmazonItemCache < ActiveRecord::Migration[7.1]
  def self.up
    # 既存のテーブル名を変更するのは大変。cache memory の意味のときは singular も可.
    execute <<EOF
ALTER TABLE resources ADD COLUMN amazon_response_item xml;

CREATE TABLE amazon_item_cache (
  id         serial PRIMARY KEY,
  asin       VARCHAR(20) NOT NULL UNIQUE,

  response   xml NOT NULL,

  created_at TIMESTAMP NOT NULL
);
EOF
  end
end
