
class ResourcesAddFields < ActiveRecord::Migration[7.1]
  def self.up
    cols = [
      ["title",            "VARCHAR(200) NOT NULL"],
      ["edition",          "VARCHAR(20)"],
      ["authors",           "VARCHAR(200) ARRAY"],
      ["book_designer",    "VARCHAR(200)"],
      ["publisher",        "VARCHAR(200)"],
      ["publication_date", "VARCHAR(12)"],
      ["genre",            "VARCHAR(200)"],
      ["country",          "CHAR(2)"] ]

    cols.each do |col_pair|
      execute <<EOF
ALTER TABLE resources ADD COLUMN #{col_pair[0]} #{col_pair[1]}
EOF
    end
  end
  
end
