
-- キャッシュ。定期的に捨てる.
CREATE TABLE amazon_item_cache (
  id         serial PRIMARY KEY,
  asin       VARCHAR(20) NOT NULL UNIQUE,

  -- <Item>要素
  response   xml NOT NULL,

  created_at TIMESTAMP NOT NULL
);
