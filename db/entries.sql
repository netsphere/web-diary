-- 一つのエントリ
-- ブログに複数ある。エントリごとに型を持つ。
CREATE TABLE entries (
  id         serial PRIMARY KEY,
  -- 親
  blog_id    int NOT NULL REFERENCES blogs (id),

  -- single table inheritance.
  -- DiaryEntry, FotoEntry, MonoEntry
  type       VARCHAR(20) NOT NULL,  

  -- タイトル
  title          VARCHAR(200) NOT NULL,

  date           DATE NOT NULL,   -- 未来もOK

  -- 本文
  body           TEXT NOT NULL,

  status         int NOT NULL,   -- 非0で公開。0でドラフト状態

  created_at     TIMESTAMP NOT NULL,
  create_user_id int NOT NULL REFERENCES users (id),

  -- 最新エントリを取りやすくするために, こちらにも not nullを付ける
  updated_at     TIMESTAMP NOT NULL,
  update_user_id int REFERENCES users (id)
);
