-- -*- coding:utf-8 -*-
-- ユーザグループ。複数のブログを持てる
CREATE TABLE user_groups (
  id          serial PRIMARY KEY,
  urlkey      VARCHAR(30) NOT NULL UNIQUE,  -- 英数のみ

  -- 表示名
  name        VARCHAR(40) NOT NULL,  -- 長い名前

  -- description TEXT NOT NULL,  -- グループの概要

  -- public_flag int NOT NULL,  -- 0で存在を公開しない

  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP
);

