# -*- mode:ruby; coding:utf-8 -*-

# Q's Web Diary
# Copyright (c) 1999-2001,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/webdiary.rhtml
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require "strstore.rb"
require "datesup.rb"
require './passwd.rb'

class Profile
  attr_accessor :userid, :mail
  attr_accessor :site_name, :lead
  # FOAF
  # http://xmlns.com/foaf/0.1/   FOAF Vocabulary Specification
  attr_accessor :nick, :homepage, :mail_public, :depiction
  attr_accessor :theme_name
  attr_accessor :default_body_type
  attr_accessor :mob_mail
end


class DiaryEntry
  TYPES = {
    "html" => "text/html; charset=UTF-8",
    "htmlbr" => "text/x-html-br; charset=UTF-8",
  }

  attr_accessor :header, :body
  def initialize()
    @header = {}
    @body = ""
  end

  ["Content-Type", "Subject", "Diary-Date", "Last-Modified", "Date"].each {|h|
    f = h.downcase.gsub("-", "_")
    define_method(f) {return @header[h]}
    define_method(f + "=") {|v| @header[h] = v}
  }
end


class DBAccessor
  class << self
    def valid_userid(userid)
      if !userid.is_a?(String)
        raise TypeError, "userid must be a String but was '#{userid.inspect}'"
      end
      raise SecurityError if userid == ""
      raise SecurityError if userid =~ /[^a-zA-Z0-9]/
      userid.untaint
    end
  end # << self

  class FilePath
    def initialize(private_dir_)
      raise TypeError if !private_dir_.is_a?(String)
      raise ArgumentError if private_dir_ == "" || private_dir_[0] != ?/

      @private_dir = private_dir_
    end

    def passwd
      return @private_dir + "/passwd"
    end

    def data_dir(userid)
      DBAccessor.valid_userid(userid)
      return @private_dir + "/users/#{userid}"
    end

    def mesid_counter()
      return @private_dir + "/mesid_counter"
    end

    def entry_fn(userid, key)
      DBAccessor.valid_userid(userid)
      raise TypeError if !key.is_a?(String)
      raise SecurityError if key =~ /[^a-zA-Z0-9]/
      key = key.dup.untaint

      return @private_dir + "/users/#{userid}/#{key}"
    end

    def entry_index()
      return @private_dir + "/entry_index"
    end

    def profile(userid)
      DBAccessor.valid_userid(userid)
      return @private_dir + "/users/#{userid}/profile"
    end

    def mob_mail()
      return @private_dir + "/mob_mail"
    end
  end # of class FilePath


  def initialize(data_dir_)
    raise TypeError if !data_dir_.is_a?(String)
    @file_path = FilePath.new(data_dir_)
    @passwd_store = PasswdStore.new(@file_path.passwd)

    if !FileTest.exist?(data_dir_ + "/users")
      Dir.mkdir(data_dir_ + "/users")
    end
  end
  attr_reader :passwd_store

  def user_exist?(userid)
    DBAccessor.valid_userid(userid)
    return @passwd_store.user_exist?(userid)
  end

  def add_user(ent)
    DBAccessor.valid_userid(ent.name)
    @passwd_store.add_user(ent)
    Dir.mkdir(@file_path.data_dir(ent.name))
  end

  def get_users()
    return @passwd_store.users().map {|u| u.dup}
  end

  def find_userid_by_mobmail(mobmail)
    StrStore.new(@file_path.mob_mail()).transaction_ro {|mobmaildb|
      mobmaildb.keys.each {|u|
        if mobmaildb[u] == mobmail
          return u
        end
      }
    }
    return nil
  end

  def get_passwd(userid)
    DBAccessor.valid_userid(userid)
    return @passwd_store.get_passwd(userid)
  end

  def update_passwd(ent, userid)
    DBAccessor.valid_userid(userid)
    raise ArgumentError if ent.name != userid  # 更新する権限があるか？
    @passwd_store.update_passwd(ent)
  end


  def get_profile userid
    DBAccessor.valid_userid(userid)

    ent = get_passwd(userid)
    if ent
      ret = Profile.new
      ret.userid = ent.name
      ret.mail = ent.mail
    else
      return nil
    end

    StrStore.new(@file_path.profile(userid)).transaction_ro {|profdb|
      ret.site_name = profdb["site_name"] if profdb.key?("site_name")
      ret.lead = profdb["lead"].split('\n').join("\n") if profdb.key?("lead")
      if profdb.key?("mail_public")
        ret.mail_public = profdb["mail_public"] == "yes" ? true : false
      end
      ret.nick = profdb["foaf:nick"] if profdb.key?("foaf:nick")
      ret.homepage = profdb["foaf:homepage"] if profdb.key?("foaf:homepage")
      ret.depiction = profdb["foaf:depiction"] if profdb.key?("foaf:depiction")
      ret.theme_name = profdb["theme_name"] if profdb.key?("theme_name")
      if profdb.key?("default_body_type") && profdb["default_body_type"] != ""
        ret.default_body_type = profdb["default_body_type"]
      end
    }
    StrStore.new(@file_path.mob_mail()).transaction_ro {|mobmaildb|
      ret.mob_mail = mobmaildb[userid] if mobmaildb.key?(userid)
    }
    return ret
  end

  # passwdファイルに記録されるものは更新しないことに注意
  def set_profile(prof, userid)
    DBAccessor.valid_userid(prof.userid)
    raise ArgumentError if prof.userid != userid

    StrStore.new(@file_path.profile(prof.userid)).transaction {|profdb|
      profdb["site_name"] = (prof.site_name || "")
      profdb["lead"] = (prof.lead || "").split(/\r?\n/).join('\n')
      profdb["mail_public"] = (prof.mail_public ? "yes" : "no")
      profdb["foaf:nick"] = (prof.nick || "")
      profdb["foaf:homepage"] = (prof.homepage || "")
      profdb["foaf:depiction"] = (prof.depiction || "")
      profdb["theme_name"] = (prof.theme_name || "")
      profdb["default_body_type"] = (prof.default_body_type || "")
    }
    StrStore.new(@file_path.mob_mail()).transaction {|mobmaildb|
      mobmaildb[userid] = (prof.mob_mail || "")
    }
  end

  # Message-IDはシステム全体で一意な値
  # とりあえず、ファイル名として有効な文字列  => '/'や'.'を含むのは良くない
  def new_mesid()
    return "m" + incr_counter(@file_path.mesid_counter()).to_s
  end
  private :new_mesid

  def valid_mesid(str)
    return str =~ /^m[0-9]+$/
  end
  

  # エントリを新規登録・更新
  def store_entry(entry, userid)
    DBAccessor.valid_userid(userid)

    if !entry.header["Message-ID"]
      entry.header["Message-ID"] = new_mesid()
      is_new = true
    else
      is_new = false
    end
    StrStore.new(@file_path.entry_index()).transaction {|indexdb|
      if is_new
        if indexdb.key?(entry.header["Message-ID"])
          raise "same key exists."
        end
      else
        if indexdb.key?(entry.header["Message-ID"]) && 
           indexdb[entry.header["Message-ID"]].split("\t")[0] != userid
          raise
        end
      end
      indexdb[entry.header["Message-ID"]] =
           [userid, entry.header["Diary-Date"], entry.header["Last-Modified"],
            entry.header["Date"]].join("\t")
    }
    File.open(@file_path.entry_fn(userid, entry.header["Message-ID"]), "w") {|fp|
      fp.flock(File::LOCK_EX)
      entry.header.each {|k, v|
        fp.print "#{k}: #{v}\n"
      }
      fp.print "\n"
      fp.print entry.body
    }
  end


  # エントリを得る
  def get_entry mesid, userid
    raise TypeError if !mesid.is_a?(String)
    DBAccessor.valid_userid(userid)

    entry = DiaryEntry.new
    begin
      File.open(@file_path.entry_fn(userid, mesid), "r") {|fp|
        fp.flock(File::LOCK_SH)
        while line = fp.gets
          line.gsub!(/[\r\n]/, '')
          break if line == ""
          k, v = line.split(/[ \t]*\:[ \t]*/, 2)
          entry.header[k] = v
        end
        entry.body = fp.read
      }
    rescue SystemCallError
      return nil
    end
    return entry
  end

  # 最新のエントリ（複数）を得る
  def get_latest_entries(size, userid)
    DBAccessor.valid_userid(userid)

    index = get_index_table(userid)
    index.sort! {|x, y| if x[1] != y[1] then y[1] <=> x[1] else y[3] <=> x[3] end}
    entries = []
    index[0 .. size - 1].each {|i|
      entries << get_entry(i[0], userid)
    }
    return entries
  end

  def get_month_entries(year, mon, userid)
    DBAccessor.valid_userid(userid)
    raise if !Date.exist?(year, mon, 1)

    index = get_index_table(userid)
    e_index = []
    index.each {|line|
      if line[1].year == year && line[1].mon == mon
        e_index << line
      end
    }
    return e_index
  end

  # [mesid, diary-date, last-modified, date]の配列を返す
  def get_index_table(userid)
    DBAccessor.valid_userid(userid)

    r = []
    StrStore.new(@file_path.entry_index()).transaction_ro {|indexdb|
      keys = indexdb.keys
      keys.each {|k|
        ent_userid, diary_date, modified, date = indexdb[k].split("\t")
        if ent_userid == userid
          r << [k, Date.parse(diary_date), get_time(modified), get_time(date)]
        end
      }
    }
    return r
  end

  # 指定の日で最新のエントリを返す
  def find_entry_by_diary_date(day, userid)
    raise TypeError if !day.is_a?(Date)
    
    index = get_index_table(userid)
    index.sort! {|x, y| if x[1] != y[1] then y[1] <=> x[1] else y[3] <=> x[3] end}
    index.each {|idx|
      return get_entry(idx[0], userid) if idx[1] == day
    }
    return nil
  end
end
