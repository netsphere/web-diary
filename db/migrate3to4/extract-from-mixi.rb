# -*- coding: utf-8 -*-

# mixiの日記データを抜き出して保存する。

# 解説サイト
# http://d.hatena.ne.jp/kitamomonga/20081209/kaisetsu_for_ver_0_9_ruby_www_mechanize

require "rubygems"
require "mechanize"
require "date"
require "csconv"


########################################################################
# 設定

LOGIN_EMAIL = "hogehoge@example.com"
LOGIN_PASSWORD = "password"

TARGET_UID = 12345678 

START_YEAR = 2007
START_MON = 10


########################################################################

agent = Mechanize.new
agent.user_agent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)"

# ログインする
page = agent.get "http://mixi.jp/"

# 2011.10.17 この時点ではクッキーが生成されない

form = page.form_with :name => "login_form"
form.field_with(:name => "email").value = LOGIN_EMAIL
form.field_with(:name => "password").value = LOGIN_PASSWORD

page = agent.submit form
puts page.title, page.uri    #=> http://mixi.jp/login.pl?from=login0

# 2011.10.17 クッキーのdomainが ".mixi.jp" になっている。
# ブラウザは対応しているが Mechanize 2.0.1 は, バグにより、送信しない。
agent.cookies.each do |cookie|
  cookie.domain = "mixi.jp"
end
agent.cookie_jar.jar["mixi.jp"] = agent.cookie_jar.jar[".mixi.jp"]

page = agent.get "http://mixi.jp/home.pl"
puts page.title, page.uri    #=> http://mixi.jp/home.pl
raise if page.uri.to_s != "http://mixi.jp/home.pl"  # DEBUG


# それぞれのエントリ。１日に複数のエントリがあることがある。
# @param page 日記エントリページ
def save_entry agent, uid, page
  puts page.uri

  /view_diary\.pl\?id=([0-9]+)/ =~ page.uri.to_s
  entry_id = $1
  title = page.at "div.listDiaryTitle dl dt"
  posted_at = page.at "div.listDiaryTitle dl dd"
  body = page.at "div#diary_body"

  # 保存
  File.open "d/" + entry_id, "w"  do |fp|
    fp.print CSConv.e2u(posted_at.to_s), "\n\n"
    fp.print CSConv.e2u(title.to_s), "\n\n"
    fp.print CSConv.e2u(body.to_s)
  end

  # 画像の一覧
  pic_links = {}
  page.search("a").each do |elem|    # links_with では上手くない
    if elem.get_attribute("onclick")
      # p elem
      if elem.get_attribute("onclick") =~ /show_diary_picture\.pl\?owner_id=[0-9]+\&id=([0-9]+)\&number=([0-9]+)/
        pic_links[$2] = elem
      end
    end
  end
#  p pic_links

  pic_links.each do |number, pic_link|
    agent2 = agent.dup
    img_page = agent2.get "http://mixi.jp/show_diary_picture.pl?owner_id=#{uid}&id=#{entry_id}&number=#{number}"

    sleep 3   # 連続取得しようとするとエラーになる

    img = agent2.get img_page.at("img").get_attribute("src")
    File.open "d/" + entry_id + "_" + number + 
                             fileext(img.header["content-type"]), "w" do |fp|
      fp.write img.body
    end
  end
end


def fileext content_type
  p content_type
  {"image/jpeg" => ".jpg",
   "image/png" => ".png",
   "image/gif" => ".gif"}[content_type] || ""
end


today = Date.today
year = START_YEAR; mon = START_MON
while true
  break if Date.new(year, mon, 1) > today

  page = agent.get "http://mixi.jp/list_diary.pl?id=#{TARGET_UID}&year=#{year}&month=#{mon}"
  sleep 3

  puts page.title, page.uri
  raise if /日記/ !~ page.title
  #puts page.inspect

  links = {}
  page.links_with(:href => /view_diary\.pl/).each do |link|
    links[ link.href.split("#", 2).first ] = link
  end

  links.each do |href, link|
    page = agent.get "http://mixi.jp/" + href
    sleep 3

    save_entry agent, TARGET_UID, page
  end

  mon += 1
  if mon > 12
    year += 1; mon = 1
  end
end
