# -*- coding:utf-8 -*-

# Q's Web Diary
# Copyright (c) 1999-2001,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/webdiary.rhtml
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require "csconv"


class Passwd
  attr_accessor(
      # SUSv3
      :name,   # User's login name.
      :passwd, # 暗号化されたパスワード   POSIXでは<pwd.h>の説明で抜けている
      :uid,    # Numerical user ID.
      :gid,    # Numerical group ID.
      :dir,    # Initial working directory.
      :shell,  # Program to use as shell.
      # glibc
      :gecos,  # Real name
      # extension
      :expire, # 有効期限  Timeインスタンス
      :mail)   # メールアドレス

  def to_s
    Passwd.arg_check(RuntimeError, self)
    s = ""
    [@name, @passwd, @uid, @gid, @gecos, @dir, @shell, (@expire ? @expire.to_i : ""),
       @mail, ""  # 最後の要素は平文パスワードだった
    ].each_with_index {|v, i|
      s += ":" if i != 0
      s += CGI.escape(u2e(v.to_s))
    }
    return s
  end

  def Passwd.parse(s)
    raise TypeError if !s.is_a?(String)

    a = s.gsub(/[\r\n]/, '').split(':')
    a.map! {|e| e2u(CGI.unescape(e))}

    o = Passwd.new
    o.name, o.passwd, o.uid, o.gid, o.gecos, o.dir, o.shell,
                              o.expire, o.mail, dummy_ = a
    o.expire = o.expire == "" ? nil : Time.at(o.expire.to_i)
    Passwd.arg_check(ArgumentError, o)
    return o
  end

  private
  def Passwd.arg_check(cls, obj)
    raise TypeError if !obj.name.is_a?(String) && !obj.name.is_a?(NilClass)
    raise TypeError if !obj.passwd.is_a?(String) && !obj.passwd.is_a?(NilClass)
    raise TypeError if !obj.gecos.is_a?(String) && !obj.gecos.is_a?(NilClass)
    raise TypeError if !obj.expire.is_a?(Time) && !obj.expire.is_a?(NilClass)
    raise TypeError if !obj.mail.is_a?(String) && !obj.mail.is_a?(NilClass)
  end
end


# ファイルにStrStoreで保存する
class PasswdStore
  def initialize(path_)
    @path = path_
  end

  # ユーザーが存在し、かつパスワードが合っていればtrueを返す。
  # 有効期限を過ぎているエントリは削除する。
  def check_passwd(userid_, pwd_)
    raise TypeError if !userid_.is_a?(String)
    raise TypeError if !pwd_.is_a?(String)

    StrStore.new(@path).transaction {|pwddb|
      # expire sweep
      now = Time.now
      roots = pwddb.keys
      roots.each {|k|
        ent = Passwd.parse(pwddb[k])
        pwddb.delete(k) if ent.expire && ent.expire < now
      }

      if !pwddb.key?(userid_)
        return false
      end
      ent = Passwd.parse(pwddb[userid_])
      if ent.passwd != Digest::MD5.new.update(pwd_).hexdigest
        return false
      end

      # ok
      ent.expire = nil
      pwddb[userid_] = ent.to_s
    }
    return true
  end

  def user_exist?(userid)
    return false if !userid
    raise TypeError if !userid.is_a?(String)

    StrStore.new(@path).transaction_ro {|pwddb|
      if pwddb.key?(userid)
        return true
      else
        return false
      end
    }
  end

  def add_user(ent)
    raise TypeError if !ent.is_a?(Passwd)
    raise RuntimeError if user_exist?(ent.name)
    StrStore.new(@path).transaction {|pwddb|
      pwddb[ent.name] = ent.to_s
    }
  end

  # エントリを削除する。
  # 削除したエントリを返す。
  def delete_user(userid)
    raise TypeError if !userid.is_a?(String)
    StrStore.new(@path).transaction {|pwddb|
      if pwddb.key?(userid)
        ent = Passwd.parse(pwddb[userid])
        pwddb.delete(userid)
        return ent
      end
    }
    return nil
  end

  def update_passwd(ent)
    raise TypeError if !ent.is_a?(Passwd)
    raise RuntimeError if !user_exist?(ent.name)
    StrStore.new(@path).transaction {|pwddb|
      pwddb[ent.name] = ent.to_s
    }
  end

  def get_passwd(userid)
    raise TypeError if !userid.is_a?(String)
    StrStore.new(@path).transaction_ro {|pwddb|
      if pwddb.key?(userid)
        return Passwd.parse(pwddb[userid])
      end
    }
    return nil
  end

  def users()
    entries = []
    StrStore.new(@path).transaction_ro {|pwddb|
      entries = pwddb.keys
    }
    return entries
  end
end
