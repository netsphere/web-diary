#!/usr/local/bin/ruby
# -*- coding: utf-8 -*-

# 保存されたmixi日記データをQ's Web Diary に投入する。

require File.dirname(__FILE__) + "/../../config/environment" 
#require "rubygems"
#require "nokogiri"
#gem "nokogiri"
require "import-common.rb"


# 末尾の"/"は付けない
PATH = "/home/hori/mixi-mizuki-backup"

#######################################################################


def import_entry blog, v4u, fname
  puts "File: " + fname 

  doc = Nokogiri::HTML.fragment File.read(fname), "UTF-8"
  # p doc.class   #=> Nokogiri::HTML::DocumentFragment

  datestr = doc.css("dd").first.content
  raise if datestr !~ /([12][0-9]+)年([0-9]+)月([0-9]+)日([0-9]+)\:([0-9]+)/
  created_at = Time.local $1, $2, $3, $4, $5
  puts "created_at: " + created_at.to_s

  subject = doc.css("dt").first.content
  puts "subject: " + subject

  body = doc.css("div#diary_body").first
  # puts body.children.to_html

  e4 = Entry.new :blog => blog,
          :title => subject, 
          :date => Date.new(created_at.year, created_at.mon, created_at.day),
          :body => body.children.to_html,
          :status => 1, # 公開
          :create_user_id => v4u.id,
          :created_at => created_at
  e4.type = "Entry"
  e4.save!

  # 添付ファイル
  Dir.glob fname + "*.jpg" do |attach|
    a = AttachFile.new :entry => e4,
           :original_filename => File.basename(attach),
           :content_type => "image/jpeg",
           :body => File.read(attach),
           :hashed_filename => Digest::SHA256.hexdigest(Time.now.to_s + attach),
           :create_user_id => v4u.id
    a.save!
  end
end


Blog.transaction do 
  Accounts::App.transaction do
    blog, v4u = create_blog "mizuki", "mizuki", nil
    # {
    # :name => "mixiの日記",
    # :lead => "",
    # :theme_filename => ""
    # }
    blog = Blog.find(1) # TODO: special

    Dir.glob PATH + "/*" do |fname|
      next if File.basename(fname) !~ /^[0-9]+$/
      import_entry blog, v4u, fname
    end
  end # transaction
end # transaction
