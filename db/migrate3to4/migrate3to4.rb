#!/usr/local/bin/ruby
# -*- coding: utf-8 -*-

# Migration from Q's Web Diary v3 to v4.

require File.dirname(__FILE__) + "/../../config/environment" 
require "diary-db.rb"
require "import-common.rb"

# 設定 #################################################################

DATA_DIR = "/home/hori/netsphere-snapshot-20110823/wwwdata/diary2"

# v3のユーザID => v4のユーザグループID(重複可)
USER_MAP = {
  "mizuki" => {:ug => "mizuki", :u => "mizuki"},
  "mizuki2" => {:ug => "mizuki", :u => "mizuki"}
}

########################################################################


DB3 = DBAccessor.new DATA_DIR

def migrate_entries v3uid, blog, v4u
  raise TypeError if !v4u

  entries3 = DB3.get_index_table v3uid
  entries3.each do |m3|
    e3 = DB3.get_entry m3[0], v3uid

    if e3.content_type.index "text/x-html-br"
      # raise e3.body.inspect # DEBUG
      body = e3.body.split(/\r?\n|\r/).collect do |line| 
               "<p>" + line + "</p>" 
             end.join
    else
      body = e3.body
    end

    e4 = Entry.new :blog => blog,
                   :title => e3.subject,
                   :date => e3.diary_date,
                   :body => body,
                   :status => 1, # 公開
                   :create_user_id => v4u.id,
                   :created_at => e3.date,
                   :updated_at => e3.last_modified
    e4.type = "Entry"
    e4.save!
  end
end


Blog.transaction do
  Accounts::App.transaction do 
    USER_MAP.each do |v3uid, v4setting|
      prof3 = DB3.get_profile v3uid

      blog, v4u = create_blog v4setting[:ug], v4setting[:u], {
        :name => prof3.site_name,
        :lead => prof3.lead,
        :theme_filename => prof3.theme_name
      }

      migrate_entries v3uid, blog, v4u
    end
  end # transaction
end # transaction
