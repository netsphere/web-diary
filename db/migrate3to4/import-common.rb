# -*- coding: utf-8 -*-

# 必要があれば, 呼出し側でtransaction で囲むこと
# @return [Array] 配列(blog, user).
def create_blog ugname, username, blog_attrs
  a_u = Accounts::User.find_by_login username
  raise if !a_u
  v4u = User.find_by_login username
  if !v4u
    v4u = User.new :name => a_u.name
    v4u.login = a_u.login
    v4u.save!
  end

  a_ug4 = Accounts::UserGroup.find_by_urlkey ugname
  raise if !a_ug4
  ug4 = UserGroup.find_by_urlkey ugname
  if !ug4
    ug4 = UserGroup.new :name => a_ug4.name
    ug4.urlkey = a_ug4.urlkey
    ug4.save!

    uug = UsersUserGroup.new :user => v4u, :user_group => ug4
    uug.save!
  end

  if blog_attrs
    blog = Blog.new( {:user_group_id => ug4.id}.merge(blog_attrs) )
    blog.save!
    a_app = Accounts::App.new :app_type => "diary",
                        :app_id => blog.id,
                        :user_group => a_ug4
    a_app.save!
  end

  return [blog, v4u]
end
