# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.1].define(version: 2017_09_17_025622) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "attach_files", id: :serial, force: :cascade do |t|
    t.integer "entry_id", null: false
    t.string "original_filename", limit: 250, null: false
    t.string "content_type", limit: 60, null: false
    t.binary "body", null: false
    t.string "hashed_filename", limit: 64, null: false
    t.datetime "created_at", precision: nil, null: false
    t.integer "create_user_id", null: false

    t.unique_constraint ["hashed_filename"], name: "attach_files_hashed_filename_key"
  end

  create_table "blogs", id: :serial, force: :cascade do |t|
    t.integer "user_group_id", null: false
    t.string "name", limit: 100, null: false
    t.string "lead", limit: 250, null: false
    t.string "theme_filename", limit: 250, null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil
    t.string "urlkey", limit: 30, null: false

    t.unique_constraint ["urlkey"], name: "blogs_urlkey_key"
  end

  create_table "comments", id: :serial, force: :cascade do |t|
    t.integer "entry_id", null: false
    t.text "body", null: false
    t.datetime "created_at", precision: nil, null: false
    t.integer "create_user_id", null: false
  end

  create_table "entries", id: :serial, force: :cascade do |t|
    t.integer "blog_id", null: false
    t.string "type", limit: 20, null: false
    t.string "title", limit: 200, null: false
    t.date "date", null: false
    t.text "body", null: false
    t.integer "status", null: false
    t.datetime "created_at", precision: nil, null: false
    t.integer "create_user_id", null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "update_user_id"
  end

  create_table "entries_resources", id: :serial, force: :cascade do |t|
    t.integer "entry_id", null: false
    t.integer "resource_id", null: false

    t.unique_constraint ["entry_id", "resource_id"], name: "entries_resources_entry_id_resource_id_key"
  end

  create_table "entries_tags", id: :serial, force: :cascade do |t|
    t.integer "entry_id", null: false
    t.integer "tag_id", null: false

    t.unique_constraint ["entry_id", "tag_id"], name: "entries_tags_entry_id_tag_id_key"
  end

  create_table "resources", id: :serial, force: :cascade do |t|
    t.integer "uri_type", null: false
    t.string "uri", limit: 200, null: false
    t.string "title", limit: 200, null: false
    t.string "edition", limit: 20
    t.string "authors", limit: 200, array: true
    t.string "book_designer", limit: 200
    t.string "publisher", limit: 200
    t.string "publication_date", limit: 12
    t.string "genre", limit: 200
    t.string "country", limit: 2
    t.xml "amazon_response_item"
    t.datetime "created_at", precision: nil, null: false

    t.unique_constraint ["uri_type", "uri"], name: "resources_uri_type_uri_key"
  end

  create_table "tags", id: :serial, force: :cascade do |t|
    t.string "name", limit: 100, null: false
    t.datetime "created_at", precision: nil, null: false
    t.integer "create_user_id", null: false

    t.unique_constraint ["name"], name: "tags_name_key"
  end

  create_table "user_groups", id: :serial, force: :cascade do |t|
    t.string "urlkey", limit: 30, null: false
    t.string "name", limit: 40, null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil

    t.unique_constraint ["urlkey"], name: "user_groups_urlkey_key"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "login", limit: 40, null: false
    t.string "name", limit: 40, null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil

    t.unique_constraint ["login"], name: "users_login_key"
  end

  create_table "users_user_groups", id: :serial, force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "user_group_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil

    t.unique_constraint ["user_id", "user_group_id"], name: "users_user_groups_user_id_user_group_id_key"
  end

  add_foreign_key "attach_files", "entries", name: "attach_files_entry_id_fkey"
  add_foreign_key "attach_files", "users", column: "create_user_id", name: "attach_files_create_user_id_fkey"
  add_foreign_key "blogs", "user_groups", name: "blogs_user_group_id_fkey"
  add_foreign_key "comments", "entries", name: "comments_entry_id_fkey"
  add_foreign_key "comments", "users", column: "create_user_id", name: "comments_create_user_id_fkey"
  add_foreign_key "entries", "blogs", name: "entries_blog_id_fkey"
  add_foreign_key "entries", "users", column: "create_user_id", name: "entries_create_user_id_fkey"
  add_foreign_key "entries", "users", column: "update_user_id", name: "entries_update_user_id_fkey"
  add_foreign_key "entries_resources", "entries", name: "entries_resources_entry_id_fkey"
  add_foreign_key "entries_resources", "resources", name: "entries_resources_resource_id_fkey"
  add_foreign_key "entries_tags", "entries", name: "entries_tags_entry_id_fkey"
  add_foreign_key "entries_tags", "tags", name: "entries_tags_tag_id_fkey"
  add_foreign_key "tags", "users", column: "create_user_id", name: "tags_create_user_id_fkey"
  add_foreign_key "users_user_groups", "user_groups", name: "users_user_groups_user_group_id_fkey"
  add_foreign_key "users_user_groups", "users", name: "users_user_groups_user_id_fkey"
end
