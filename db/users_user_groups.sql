-- -*- coding:utf-8 -*-
-- relationship of users and user_groups
CREATE TABLE users_user_groups (
  id            serial PRIMARY KEY,
  user_id       int NOT NULL REFERENCES users (id),
  user_group_id int NOT NULL REFERENCES user_groups (id),

  -- role          int NOT NULL,  -- 20=管理者、10=一般ユーザ。グループごとに設定する
  created_at    TIMESTAMP NOT NULL,
  updated_at    TIMESTAMP,
  UNIQUE (user_id, user_group_id)
);
