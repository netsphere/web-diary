# -*- coding:utf-8 -*-

# Q's Web Diary
# Copyright (c) 2001,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


Rails.application.routes.draw do
  resources :user_groups 
  
  # 〜について
  resources :resources do
    collection do  # id不要
      get 'new_book'
      post 'search'
    end
  end

  # 公開プロフィール
  resources :users, :param => :login

  # blogから外だし. => ブログをまたぐ
  resources :tags

  resources :blogs, :param => 'urlkey' do
    match 'admin(/:action)', :controller => 'blog_admin', :via => [:get, :post]

    resources :entries do
      match "preview" => "entries#preview",
            :via => [:get]
      match "set_private" => "entries#set_private",
            :via => [:post]
      match "set_public" => "entries#set_public",
            :via => [:post]

      resources :attaches
    end

    resources :tags
    
    # 1ヶ月分
    match ":year-:mon" => "blogs#month", 
          :constraints => {:year => /[12][0-9][0-9][0-9]/, :mon => /[01][0-9]/},
          :via => [:get]
  end

  resource :account do
    match "login" => "account#login", :via => [:get]
  end

  root :to => "welcome#index"

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # アカウント
  match 'account(/:action(/:id(.:format)))',
        :via => [:get, :post, :patch],
        :controller => "account"

  # OmniAuth対応
  match '/auth/:provider/callback' => 'auth#callback', :via => [:get, :post]
  
end # draw()
