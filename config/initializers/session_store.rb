# -*- coding:utf-8; mode:ruby -*-
# Be sure to restart your server when you modify this file.

#Rails.application.config.session_store :cookie_store, key: '_WebDiary_session'

require "rails_sup/rails_session_manager"
require "wafu2/session/file_storage"

# セッション情報はサーバ側で保存.
Rails.application.configure do |app|
  app.config.session_store Wafu::Session::RailsSessionManager,
        :database_manager => {
          :class => Wafu::Session::FileStorage,
          :dir => Rails.root.to_s + "/tmp/session", # ファイルは共有しない
          :holdtime => (60 * 60) * 3
        },
        :key => 'nsdiary_sid',
        :path => "/"
        #:domain => "kiwi.fruits"
end # configure

