ENV["BUNDLE_GEMFILE"] ||= File.expand_path("../Gemfile", __dir__)

require "bundler/setup" # Set up gems listed in the Gemfile.
require "bootsnap/setup" # Speed up boot time by caching expensive operations.

# `config/initializers` の前に行うことは, ここではなく, `application.rb` に書くこと.
# ここは, `Rails.root` すら準備がない.

