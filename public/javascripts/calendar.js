'use strict';

var today;     // 今日
var cur_first; // 現在月の1日
var week = new Array('日', '月', '火', '水', '木', '金', '土');
var subWin;
var year_obj;
var mon_obj;
var day_obj;

/**
 * カレンダーHTMLの生成
 */
function getCalendarHtml() {
  var start_day = new Date(cur_first - (cur_first.getDay() * 1000 * 60 * 60 * 24));
  var title = cur_first.getFullYear() + "年" + (cur_first.getMonth() + 1) + "月";

  var ddata  = '<html><head>\
  <title>' + title + '</title>\
  <style type="text/css">html,body,form,table {padding:0; margin:0;}</style>\
  <script type="text/javascript">function f() { self.window.focus(); }</script>\
</head>\
<body>\
  <form>\
    <table BGCOLOR="#dddddd" width="100%" \
           style="font-family:sans-serif;font-size:10pt;text-align:right">\n';

  // 月
  ddata += '<tr BGCOLOR="#6699ff">\
  <th colspan="7" align="center" nowrap>\
    <input type="button" value="&lt;&lt;" \
           onClick="window.opener.rewriteCalendar(-1)" style="padding:0" />\
    <span style="font-size:11pt">' + title + '</span>\
    <input type="button" value="今月" \
           onClick="window.opener.rewriteCalendar(0)" style="padding:0">\
    <input type="button" value="&gt;&gt;" \
           onClick="window.opener.rewriteCalendar(1)" style="padding:0">\
</tr>\n';

  // 週
  ddata += '<tr BGCOLOR="#00cccc">\n'
  for (var i = 0; i < 7; i++) {
    ddata += '<TH WIDTH=14>\n'
    ddata += week[i]
  }
  ddata += '</TR>\n'

  // 日
  for (var j = 0; j < 6; j++) {
    ddata += '<tr BGCOLOR="#eeeeee">\n'
    for (var i = 0; i < 7; i++) {
      var d = new Date(start_day.getTime() + ((j * 7 + i) * 1000 * 60 * 60 * 24));
      if (d.getFullYear() == today.getFullYear() && 
          d.getMonth() == today.getMonth() &&
          d.getDate() == today.getDate()) {
        ddata += '<TD BGCOLOR="#ff99ff" WIDTH="14">\n'
      }
      else if (d.getMonth() != cur_first.getMonth()) {
        ddata += '<TD BGCOLOR="#cccccc" WIDTH="14">\n'
      }
      else {
        ddata += '<td width="14">\n'
      }
      ddata += '<a href="javascript:window.opener.setValue(' + d.getFullYear() + ',' +
                 d.getMonth() + ',' + d.getDate() + ')">'
      ddata += d.getDate() 
      ddata += '</a>\n'
    }
    ddata += '</tr>\n'
  }

  // ステータス行
  ddata += '  <tr>\
    <td colspan="7" align="center">\
      <input type="button" value="キャンセル" \
             onClick="window.opener.closeCalendar();">\
  </tr>\
</table>\
</form></body></html>\n';

  return ddata;
}


/**
 * カレンダーウィンドウを開く
 */
function openCalendar(year_obj_, mon_obj_, day_obj_, ev, dateType) {
  today = new Date();
  cur_first = new Date(today.getFullYear(), today.getMonth(), 1);
  year_obj = year_obj_;
  mon_obj = mon_obj_;
  day_obj = day_obj_;

  var left = ev.screenX + 10;
  var top = ev.screenY - 50;

  subWin = window.open("", "calendar", 'width=190,height=195,left=' + left + ',top=' + top);
  subWin.document.write(getCalendarHtml());
  subWin.document.close();
  subWin.f();
}

function closeCalendar() {
  subWin.close();
}


function rewriteCalendar(direction) {
  var y = cur_first.getFullYear();
  var m = cur_first.getMonth();
  if (direction == -1) {
    // 前月
    if (m == 0) { 
      y -= 1; m = 11; 
    } 
    else 
      m -= 1;
  }
  else if (direction == 0) {
    y = today.getFullYear();
    m = today.getMonth();
  }
  else {
    // 次月
    if (m == 11) { 
      y += 1; m = 0; 
    } 
    else 
      m += 1;
  }
  cur_first = new Date(y, m, 1);
  subWin.document.open();
  subWin.document.write(getCalendarHtml());
  subWin.document.close();
}


function setValue(y, m, d) {
  if (year_obj) { year_obj.value = y; }
  mon_obj.value = m + 1;
  day_obj.value = d;
  subWin.close();
}

