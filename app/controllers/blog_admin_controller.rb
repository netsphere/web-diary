# -*- coding:utf-8 -*-

# Q's Web Diary
# Copyright (c) 2001,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 管理者用ページ
class BlogAdminController < ApplicationController
  before_filter :set_blog #, only: [:show, :month, :edit, :update, :destroy]
  
  # before_filter :login_required
  before_filter :check_right_to_access_blog


  # ドラフトの一覧
  def index
    #@blog = Blog.find params[:blog_id]
    @entry = Entry.new :blog => @blog
  end


  def select_theme
    #@blog = Blog.find params[:blog_id]
    @entry = Entry.new :blog => @blog,
                       :title => "タイトル。タイトル。タイトル。タイトル。タイトル。",
                       :date => Date.today,
                       :body => "これは本文。これは本文。これは本文。これは本文。これは本文。これは本文。これは本文。これは本文。これは本文。\nこれは本文。これは本文。これは本文。\nこれは本文。これは本文。これは本文。これは本文。これは本文。これは本文。これは本文。これは本文。これは本文。これは本文。これは本文。\n",
                       :created_at => Time.now,
                       :create_user_id => current_user.id
    @entry.abouts << (About.new :uri => "http://example.com/................")
    @entry.tags << (Tag.new :name => "テストのタグ")

    @themes = []
    Dir.glob( WebDiary::Application.config.theme_local_dir + "/*" ) do |path|
      name = File.basename path
      if FileTest.directory?(path) && FileTest.file?(path + "/#{name}.css")
        @themes << [name, name]
      end
    end

    render :layout => "tdiary"
  end


  def update_theme
    #@blog = Blog.find params[:blog_id]
    @blog.theme_filename = params[:theme]
    @blog.save!

    flash[:notice] = "テーマを変更しました。"
    redirect_to :action => "index"
  end

private ##################################################

  # Use callbacks to share common setup or constraints between actions.
  def set_blog
    @blog = Blog.find_by_urlkey params[:blog_urlkey]
  end

end # class BlogAdminController
