# -*- coding: utf-8 -*-

# Q's Web Diary
# Copyright (c) 2001,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #protect_from_forgery with: :exception

  # current_user など  
  #include AuthenticatedSystem::ControllerHelper

  #include ApplicationHelper
  
  # remember me
  before_action :login_from_cookie

  
private ########################################

  # @override
  def redirect_to_login
    # request is a ActionDispatch::Request (actionpack package)
    # Rack::Request の派生クラス
    
if $ENABLE_SSO    
    redirect_to Rails.application.config.accounts_app_url + 
              "/account/sso?return_to=" + Rails.application.config.redirect_uri
else
    # fullpath() は query_string 付き. 
    session[:return_to] = request.fullpath()
    flash[:alert] = "ログインしてください。"
    redirect_to :controller => "account", :action => "login"
end 
  # return false
  end


  # for before_filter
  # login_required を兼ねる
  def check_right_to_access_blog
    if !current_user
      flash[:alert] = "管理ページに進もうとしています。ログインしてください。"
      redirect_to_login
      return false
    end

    raise "internal error: need @blog" if !@blog

    if !@blog.user_group.member?(current_user)
      flash[:alert] = "User #{current_user.login} is forbidden to access this blog."
      render :status => :forbidden, :text => "no right to access"
      return false
    end
  end

end # ApplicationController
