# -*- coding: utf-8 -*-

# Q's Web Diary
# Copyright (c) 2001,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# エントリの添付ファイル
class AttachesController < ApplicationController

  def index
    @blog = Blog.find params[:blog_id]
    @entry = Entry.find :first, 
                        :conditions => ["id = ? AND blog_id = ?", 
                                        params[:entry_id], @blog.id]

    if (@entry.status == 0 || @entry.date > Date.today) &&
                                           !blog_must_be_owned_by_ug
      return   # forbidden rendered
    end

    @attaches = AttachFile.find :all, :conditions => ["entry_id = ?", @entry.id]
  end


  def show
    @blog = Blog.find params[:blog_id]
    @entry = Entry.find :first, 
                        :conditions => ["id = ? AND blog_id = ?", 
                                        params[:entry_id], @blog.id]

    if (@entry.status == 0 || @entry.date > Date.today) &&
                                           !blog_must_be_owned_by_ug
      return   # forbidden rendered
    end

    @attach = AttachFile.find :first, 
                            :conditions => ["id = ? AND entry_id = ?",
                                            params[:id], @entry.id]

    path = Rails.root.to_s + "/tmp/cache/" + @attach.hashed_filename
    if !FileTest.exist?(path)
      File.open path, "wb" do |fp|
        fp.write @attach.body
      end
    end

    send_file path, :type => @attach.content_type, :disposition => "inline"
  end

end
