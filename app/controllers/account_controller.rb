# -*- coding:utf-8 -*-

require "rails_sup/authenticated_system/client/shared_auth_token"

# 認証関係
class AccountController < ApplicationController

  before_filter :login_required,
         :except => [ :login, :sso_callback ]

  # ログイン画面
  def login
if $ENABLE_SSO
    raise "internal error"
end  
  end


if $ENABLE_SSO
  def sso_callback
    if cookies[:sso_auth_token].blank?
      raise "internal error"
    end
    login = AuthenticatedSystem::Client::SharedAuthToken.verify_token cookies[:sso_auth_token], "diary"
    if !login
      redirect_to :action => 'login'
      return
    end
    
    user = ::User.find_by_login(login)
    ac_user = Accounts::User.find_by_login(login)
    if !user
      # Just-in-Time User Provisioning
      user = User.new
      user.login = ac_user.login
      user.name = ac_user.name
      user.save!
    end
    # ユーザグループの調整
    ac_ug_keys = []
    ac_user.user_groups.each do |ac_ug|
      ac_ug_keys << ac_ug.urlkey
      ug = ::UserGroup.find_by_urlkey(ac_ug.urlkey)
      if !ug
        ug = ::UserGroup.new
        ug.urlkey = ac_ug.urlkey
        ug.name = ac_ug.name
        ug.save!
      end
      if !::UsersUserGroup.where('user_id = ? AND user_group_id = ?',
                                 user.id, ug.id).first
        uug = ::UsersUserGroup.new
        uug.user_id = user.id
        uug.user_group_id = ug.id
        uug.save!
      end
    end
    # 削る
    user.user_groups.each do |r|
      if !ac_ug_keys.include?(r.urlkey)
        ::UsersUserGroup.delete_all ['user_id = ? AND user_group_id = ?',
                                     user.id, r.id]
      end
    end
    
    set_current_user user
    flash[:notice] = 'ログインしました'
    redirect_to :controller => 'welcome', :action => 'index'
  end
end # if $ENABLE_SSO

end # class AccountController
