# -*- coding:utf-8 -*-

# ユーザ
class UsersController < ApplicationController
  before_filter :set_user, only: [:show, :edit, :update, :destroy]

  before_filter :check_right_to_update_user,
                :only => [:new, :create, :edit, :update, :destroy]

=begin
  ユーザの一覧表示は不要

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end
=end


  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    raise # TODO: impl. ユーザプロビジョニング
    
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


private #############################################

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find_by_login params[:login]
    if !@user
      render :text => "Not found: user #{params[:login]}", :status => 404
    end   
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def user_params
    params.fetch(:user, {})
  end

  # for before_filter
  def check_right_to_update_user
    if current_user.login != @user.login
      render :text => "forbidden", :status => :forbidden
    end
  end
  
end # class UsersController
