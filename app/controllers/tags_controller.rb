# -*- coding:utf-8 -*-

# Q's Web Diary
# Copyright (c) 2001,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# エントリに付けるタグ
# ブログのなかの tags URL と /tagsがある.
class TagsController < ApplicationController
  before_action :set_blog

  before_action :set_tag, only: [:show, :edit, :update, :destroy]

  #layout 'blog' #, :only => [:show, :preview]

  
  # GET /tags
  # GET /tags.json
  # タグの一覧 (公開ページのみ)
  def index
    @ets = Tag.public_tags(@blog)
    if @blog
      render :layout => 'blog'
    end
  end


  # GET /tags/1
  # GET /tags/1.json
  # あるタグを含むエントリの一覧 (公開ページのみ)
  def show
    ents = Entry.select("DISTINCT entries.*") \
                 .joins("LEFT JOIN entries_tags ON entries.id = entries_tags.entry_id") \
                 .order('entries.date DESC') \
                 .paginate(:page => params[:page], :per_page => 20)
    if @blog
      @entries = ents.where("blog_id = ? AND entries.status <> 0 AND entries.date <= ? AND entries_tags.tag_id = ?", 
                            @blog.id, Date.today, @tag.id)
      @blog_tags = Tag.public_tags(@blog)
      @blog_members = @blog.members
      
      render :layout => 'blog'
    else
      @entries = ents.where("entries.status <> 0 AND entries.date <= ? AND entries_tags.tag_id = ?", 
                            Date.today, @tag.id)
    end
  end


private ###################################################

  def set_blog
    @blog = params[:blog_urlkey].blank? ? nil :
                                  (Blog.find_by_urlkey params[:blog_urlkey])
  end
  
  # Use callbacks to share common setup or constraints between actions.
  def set_tag
    @tag = Tag.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def tag_params
    params.fetch(:tag, {})
  end

end # TagsController
