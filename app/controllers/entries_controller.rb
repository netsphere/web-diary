# -*- coding:utf-8 -*-

# Q's Web Diary
# Copyright (c) 2001,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 一つ一つのエントリ
class EntriesController < ApplicationController
  before_action :set_blog

  # blog vs. entry check を兼ねる
  before_action :set_entry,
                only: [:show, :preview, :edit, :update, :destroy,
                       :set_private, :set_public]
  
  # login_required を兼ねる
  before_filter :check_right_to_access_blog,
                :except => [:show]

  layout 'blog', :only => [:show, :preview]

  
  def index
    redirect_to :controller => 'blogs', :action => 'show', :id => @blog
  end

  
  # GET /entries/1
  # GET /entries/1.json
  # 一般に公開
  def show
    if @entry.status == 0 || @entry.date > Date.today
      if !current_user
        redirect_login
        return
      else
        render :text => "forbidden", :status => :forbidden
        return
      end
    end
    @blog_tags = Tag.public_tags(@blog)
    @blog_members = @blog.members
  end


  # ドラフト原稿のプレビュー
  def preview
    @layout_entries = true
  end


  # GET /entries/new
  # 新しいエントリ
  def new
    @entry = Entry.new :blog => @blog

    #@about = Resource.new
    @tags = []
    @cand_tags = Tag.joins("JOIN entries_tags ON tags.id = entries_tags.tag_id JOIN entries ON entries_tags.entry_id = entries.id") \
                   .where("entries.blog_id = ?", @blog.id)
  end


  # トランザクションは呼び出し側で掛けること.
  # @param [Array of String] param タグ文字列の配列
  def update_tags! param
    if !param.is_a?(Array)
      raise TypeError, "param must be aArray, but #{param.class}" 
    end
    raise ArgumentError if !@entry.id
    
    EntriesTag.delete_all( ["entry_id = ?", @entry.id] ) 
    
    param.each do |tname|
      tname = normalize_kc(tname).gsub(/[\t\r\n]+/, " ").strip
      next if tname == ''
      
      tag = Tag.find_by_name(tname)
      if !tag
        tag = Tag.new :name => tname,
                      :created_by => User.find_by_login(current_user.login)
        tag.save!
      end
      et = EntriesTag.new :entry_id => @entry.id, :tag_id => tag.id
      et.save!
    end
  end
  private :update_tags!


  # POST /entries
  # POST /entries.json
  # 新しいエントリ作成 (実行)
  def create
    @entry = Entry.new params.require(:entry).permit(:date, :title, :body)
    @entry.blog = @blog
    @entry.type = "Entry"
    @entry.status = 0 # draft
    @entry.create_user_id = current_user.id

    @tags = params["tag"] || []

    begin
      Entry.transaction do 
        @entry.save!
        #update_resources! uri if uri
        update_tags!( @tags )
      end # transaction
    rescue ActiveRecord::RecordInvalid
      render :action => "new"
      return
    end

    if params[:draft]
      flash[:notice] = "新しいエントリを作成しました。"
      redirect_to :controller => 'blog_admin', :blog_urlkey => @blog.urlkey
    else
      flash[:confirm] = true
      redirect_to entry_path(@entry)
    end
  end


  # GET /entries/1/edit
  def edit
    @tags = Tag.joins("JOIN entries_tags ON tags.id = entries_tags.tag_id") \
               .where("entry_id = ?", @entry.id)
    # 自ブログで使われているもの.
    @cand_tags = Tag.joins("JOIN entries_tags ON tags.id = entries_tags.tag_id JOIN entries ON entries_tags.entry_id = entries.id") \
               .where("entries.blog_id = ?", @blog.id)

    ea = EntriesResource.where("entry_id = ?", @entry.id).first

    # ドラフトのときはプレビュー画面に戻る
    @back_path = entry_path(@entry)
  end


  # PATCH/PUT /entries/1
  # PATCH/PUT /entries/1.json
  def update
    ea = EntriesResource.find_by_entry_id @entry.id
    #@about = ea ? ea.resource : Resource.new

    @entry.attributes = params.require(:entry).permit(:date, :title, :body) 
    @entry.status = 0 # draft

    @tags = params['tag'] || []
    
    begin
      Entry.transaction do 
        # clean upのときに遅延評価させない
        #old_abouts = @entry.resources.collect do |x| x end
        old_tags = @entry.tags.collect do |x| x end

        # 一度消して作り直す
        EntriesResource.delete_all ["entry_id = ?", @entry.id]

        @entry.save!
        #update_resources! uri if uri
        update_tags!( @tags )

        # clean up
=begin        
        old_abouts.each do |a| 
          a.destroy if !EntriesResource.find_by_resource_id(a.id)
        end
=end
        old_tags.each do |t|
          t.destroy if !EntriesTag.find_by_tag_id(t.id)
        end
      end
    rescue ActiveRecord::RecordInvalid
      render :action => "edit"
      return
    end

    if params[:draft]
      flash[:notice] = 'Article was successfully updated.'
      redirect_to :controller => 'blog_admin', :action => 'index', :blog_urlkey => @blog.urlkey
    else
      flash[:confirm] = true
      redirect_to :action => 'preview', :entry_id => @entry.id
    end
  end


  # DELETE /entries/1
  # DELETE /entries/1.json
  def destroy
    @entry.destroy
    redirect_to( {:controller => 'blog_admin', :action => 'index',
                  :blog_urlkey => @blog.urlkey},
                 entries_url, notice: 'Entry was successfully destroyed.')
  end


  def set_private
    @entry.status = 0
    @entry.save!
    flash[:notice] = "エントリをドラフト(非公開)化しました。"
    redirect_to :controller => 'blog_admin', :action => 'index', :blog_urlkey => @blog.urlkey
  end


  def set_public
    @entry.status = 1
    @entry.save!
    if @entry.date <= Date.today
      flash[:notice] = "エントリを公開しました。"
      redirect_to entry_path(@entry)
    else
      flash[:notice] = "未来日付のエントリです。"
      redirect_to :controller => 'blog_admin', :action => 'index', :blog_urlkey => @blog.urlkey
    end
  end


  private ###################################################################

  def set_blog
    @blog = Blog.find_by_urlkey params[:blog_urlkey]
  end

  
  # Use callbacks to share common setup or constraints between actions.
  def set_entry
    raise "internal error" if !@blog
    
    begin
      @entry = Entry.find( params[:entry_id] || params[:id] )
    rescue ActiveRecord::RecordNotFound
      render :status => :not_found, :text => "Not found: blog_id = #{params[:blog_id]} and entry_id = #{params[:id]}"
      return false
    end
    
    if @entry.blog_id != @blog.id
      render :status => :not_found, :text => "Not found: blog_id #{params[:blog_id]} doesn't have entry_id #{params[:id]}"
      return false
    end
  end

  # Never trust parameters from the scary internet, only allow the white
  # list through.
  def entry_params
      params.fetch(:entry, {})
  end

end # class EntriesController
