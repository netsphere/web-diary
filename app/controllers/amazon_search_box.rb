# -*- coding:utf-8 -*-

USE_VACUUM = true


# Amazon で検索
# See http://kasei-san.hatenablog.com/entry/2015/08/26/205221
class AmazonSearchBox
  include ActiveModel::Model

  # サーチインデックス (カテゴリ)
  attr_accessor :search_index
  
  attr_accessor :keywords

  # 書評サイトでは意味ない
  #attr_accessor :availability
  
  # 高々一つしか指定できない。'All'の場合は挙動が変わる。意味ない
  #attr_accessor :condition

  # 著者
  attr_accessor :author

  # TODO: author があるときはブランク可
  validates :keywords, presence: true

  # 国によって (かなり) 異なる.
  # API version '2013-08-01'
  # See http://docs.aws.amazon.com/AWSECommerceService/latest/DG/SearchIndices.html
  #     http://docs.aws.amazon.com/AWSECommerceService/latest/DG/LocaleJP.html
  SEARCH_INDEX = [
    ['All',          'すべてのカテゴリ', nil], # 特別。keywords のみ有効
                                       # 5ページしか得られない
    ['Apparel',      '服&ファッション小物', 361299011],
    ['Appliances',   '大型家電'],
    ['Automotive',   '車・バイク用品'],
    ['Baby',         'ベビー&マタニティ'],
    ['Beauty',       'Beauty'],
    ['Blended',      'Blended', nil], # 特別. keywords のみ有効
                  # DVD, Electronics, Toys, VideoGames, PCHardware, Tools, SportingGoods, Books, Software, Music, GourmetFood, Kitchen, and Apparel 
    ['Books',        '本'],

    ['CreditCards',  'クレジットカード'],

    ['Electronics',  '家電&カメラ'],
    ['ForeignBooks', '洋書'],
    ['GiftCards',    'ギフト券'],
    ['Grocery',      '食品・飲料・酒'],
    ['HealthPersonalCare', 'ドラッグストア'],
    ['Hobbies',      'Hobbies'],
    ['HomeImprovement', 'DIY・工具'],
    ['Industrial',   '産業・研究開発用品'],
    ['Jewelry',      'Jewelry'],
    ['KindleStore',  'Kindleストア'],
    ['Kitchen',      'ホーム&キッチン'],
    ['Marketplace',  'Marketplace', nil],   # 特別
    ['MobileApps',   'Androidアプリ'],
    ['MP3Downloads', 'デジタルミュージック', 2129039051],
    ['Music',        '音楽'],
        #['Classical',  'クラシック音楽'],
        #['DigitalMusic', DigitalMusic],
        #['MusicTracks', ]

    ['MusicalInstruments', '楽器'],
    ['OfficeProducts', '文房具・オフィス用品'],
    ['PCHardware',   'PC・周辺機器'],
    ['PetSupplies',  'ペット用品'],
    ['Shoes',        'シューズ&バッグ'],
    ['Software',     'PCソフト'],
    ['SportingGoods', 'スポーツ&アウトドア'],
    ['Toys',         'おもちゃ', 13299551],

    ['Video',        'DVD', 561972],
        #['DVD',         'DVD'],
        #['VHS',
    ['VideoDownload', 'Amazonインスタント・ビデオ'],
    ['VideoGames',   'TVゲーム'],
    ['Watches',      '腕時計' ]
  ]


if USE_VACUUM
  # Vacuum::Response には, #parse(str) というメソッドを持つオブジェクトを渡さ
  # なければならない. #call() ちゃうんか.
  class NokogiriParser
    def parse str
      Nokogiri::XML(str)
    end
  end

  Vacuum::Response.parser = NokogiriParser.new
end # USE_VACUUM


=begin
'Large'
  Accessories  -- five ASINs and titles of accessories  いらない
  BrowseNodes  -- 付ける
  Medium
    EditorialReview  -- 付ける。ないこともある.
    Images           -- 付ける.
    ItemAttributes  -- 付ける
    OfferSummary   -- いらない
    Request    -- errorメッセージはここに入る。必須
    SalesRank   -- いらない
    Small  
      Actor
      Artist
      ASIN
      Author
      CorrectedQuery
      Creator
      Director
      Keywords
      Manufacturer
      Message
      ProductGroup
      Role
      Title
      TotalPages
      TotalResults
  Offers  -- 最低価格など. いらない
  Reviews   -- Each iframe URL is valid for 24 hours.  いらない
  Similarities  -- いらない  => 付けただけでは曖昧検索にならない.
  Tracks    -- 付ける
=end

  # @return 検索結果の本のリスト
  # @raise [ActiveRecord::RecordInvalid] 妥当でないとき例外
  def search!
    if invalid?
      raise ActiveRecord::RecordInvalid.new(self)
    end

if USE_VACUUM
    request = Vacuum.new('JP')
    request.configure(AMAZON_OPTIONS[:config])

    query = {
      'Keywords' => @keywords,
      'SearchIndex' => @search_index, 
      'ResponseGroup' => 'BrowseNodes,Medium,Tracks,Similarities',
    }
    if !['all', 'blended','watches'].include?(@search_index.downcase)
      query['Power'] = "Not kindle" 
      query['Author'] = @author if !@author.blank?
    end

    retry_count = 0
    begin
      # 戻り値の型は Vacuum::Response
      response = request.item_search(query: query)
    rescue Excon::Error::ServiceUnavailable # 503
      retry_count += 1
      if retry_count <= 2
        sleep(2)
        retry
      end
      raise
    end

if !Rails.env.production?    
    File.open(Rails.root.to_s + '/tmp/' + Time.now.to_s, 'w') do |fp|
      fp.write response.body
    end  # DEBUG
end

    xml = response.parse
    res_items_node = xml.css('ItemSearchResponse Items').first # <Items>単一
    search_request = res_items_node.css('Request ItemSearchRequest').first
    # 見つからなかった時は
    # <Error>
    #   <Code>AWS.ECommerceService.NoExactMatches</Code>
    #   <Message>リクエストに該当する結果がありません。</Message>
    # </Error>
    error_ary = res_items_node.css('Request Errors Error')
    if !error_ary.empty?
      return [search_request, nil, error_ary]
    end
    
    return [search_request, res_items_node, [] ]
    
else # amazon-ecs    
    opts = {
      # 商品カテゴリ. 国によって違う. 'All' もあり.
      search_index: @search_index, 
      #dataType: 'script',
      response_group: 'Medium', #'ItemAttributes,Images', 
      country:  'jp',
    }
    if !['all', 'blended'].include?(@search_index.downcase)
      opts[:power] = "Not kindle" 
      opts[:author] = @author if !@author.blank?
    end
=begin
    opts[:availability] = 'Available' if @availability.to_i != 0
    opts[:condition] = @condition if @condition && !@condition.empty?
=end

    # 戻り値の型は Amazon::Ecs::Responce
    books = Amazon::Ecs.item_search(
        @keywords, opts )
      
#    begin
      # オブジェクトの取得
#      books = Amazon::Ecs.item_search( # ｵﾍﾟﾚｲｼｮﾝ
#        @keywords, opts )
#    rescue Amazon::RequestError => e
      # HTTP Response: 503 Service Unavailable - AWS Access Key ID: 文字列.
      # You are submitting requests too quickly. Please retry your requests
      # at a slower rate
      #
      # サーバのうち一部が落ちている場合も同じエラー. 1回だけはretryする
      # Amazon::RequestError は 503かどうか取り出せない.
#      sleep 2
#      books = Amazon::Ecs.item_search( # ｵﾍﾟﾚｲｼｮﾝ
#        @keywords, opts )
#    end
    
end # USE_VACUUM

    return books
  end
end # class AmazonSearchBox
