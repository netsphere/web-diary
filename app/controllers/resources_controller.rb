# -*- coding:utf-8 -*-

# Q's Web Diary
# Copyright (c) 2001,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 〜について
class ResourcesController < ApplicationController
  before_action :set_resource, only: [:show, :edit, :update, :destroy]

  before_action :check_right_to_update,
                :only => [:edit, :update, :destroy]
  
  # GET /subjects
  # GET /subjects.json
  def index
    @resources = Resource.paginate(:page => params[:page], :per_page => 20)
    #@search_box = AmazonSearchBox.new
  end

  # GET /subjects/1
  # GET /subjects/1.json
  def show
    @entries = Entry.joins('JOIN entries_resources ON entries.id = entries_resources.entry_id') \
                      .where('status <> 0 AND date <= ? AND resource_id = ?', Date.today, @resource.id) 
  end


  # エントリと結びつけて, 本を追加する
  # GET /subjects/new
  def new_book
    @entry = Entry.find params[:entry]
    if !current_user || !@entry.blog.member?(current_user)
      render :text => 'forbidden to add a book', :status => :forbidden
      return
    end

    @resource = Resource.new
    @search_box = AmazonSearchBox.new
  end


  # new_book -> 検索実行
  def search
    raise "internal error" if !request.post?
    
    pa = params.require(:amazon_search_box)
    @search_box = AmazonSearchBox.new(
                               pa.permit(:keywords, :author, :search_index) )
    if !params[:entry].blank?
      @entry = Entry.find params[:entry]
    end
    @resource = Resource.new

    begin
      # element, element, array
      search_request, items_elem, error_ary = @search_box.search!
    rescue ActiveRecord::RecordInvalid
      render :action => :new_book
      return
    rescue Excon::Error::ServiceUnavailable => err # 503
      @error = [err.inspect]
      render :action => :new_book
      return
    end

    if !error_ary.empty?
      # Error element の配列.
      @error = error_ary.map do |error|
        error.css('Code').first.text + ': ' + error.css('Message').first.text
      end
      render :action => :new_book
      return
    end
    
    # 本のタイトル,画像URL, 詳細ページURLの取得
    @books = []
    AmazonItemCache.transaction do 
      items_elem.css('Item').each do |item|
        book = Resource.amazon_book item
        @books << book

        AmazonItemCache.delete_all ["asin = ?", book[:asin]]
        aic = AmazonItemCache.new(:asin => book[:asin],
                                  :response => item)
        aic.save!
      end
    end # transaction
  end
  
  
  # GET /subjects/1/edit
  def edit
  end


  # エントリに紐づける
  # POST /subjects
  # POST /subjects.json
  def create
    @entry = Entry.find params[:entry]
    aic = AmazonItemCache.find_by_asin params[:book]

    @resource = Resource.where('uri_type = 2 AND uri = ?', aic.asin).first
    if !@resource
      @resource = Resource.new(:uri_type => 2,
                               :uri => aic.asin)
    end
    @resource.amazon_response_item = aic.response
    book = Resource.amazon_book(Nokogiri::XML(aic.response).root)
    
    @resource.title = book[:title]
    @resource.authors = book[:authors]
    @resource.publisher = book[:publisher]
    @resource.publication_date = book[:PublicationDate]

    begin
      Resource.transaction do 
        @resource.save!
        er = EntriesResource.new(:entry_id => @entry.id,
                               :resource_id => @resource.id)
        er.save!
      end # transaction
    rescue
      render :new
      return
    end
    
    redirect_to( entry_path(@entry),
                 notice: 'Subject was successfully created.' )
  end

  
  # PATCH/PUT /subjects/1
  # PATCH/PUT /subjects/1.json
  def update
    if @resource.update(
            resource_params.permit(:title, :edition, :author, :book_designer,
                                   :publisher, :publication_date, :genre,
                                   :country) )
      redirect_to @resource, notice: 'Subject was successfully updated.'
    else
      render :edit
    end
  end

  
  # DELETE /subjects/1
  # DELETE /subjects/1.json
  def destroy
    @resource.destroy
    redirect_to( {:action => 'index'},
                 notice: 'Subject was successfully destroyed.' )
  end

  
private ###################################################

  # for before_filter
  def check_right_to_update
    if !(current_user && current_user.superuser)
      render :text => 'forbidden to update', :status => :forbidden
    end
  end
  
  
  # Use callbacks to share common setup or constraints between actions.
  def set_resource
    @resource = Resource.find params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def resource_params
    params.fetch(:resource, {})
  end
end
