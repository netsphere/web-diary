# -*- coding:utf-8 -*-

# Q's Web Diary
# Copyright (c) 2001,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# ブログ
# このコントローラは公開ページを扱う. 管理ページは adminコントローラ.
class BlogsController < ApplicationController
  before_action :set_blog, only: [:show, :month, :edit, :update, :destroy]

  before_action :check_right_to_access_blog,
                :only => [:new, :create, :edit, :update, :destroy]

  layout 'blog', :except => [:index]

  # GET /blogs
  # GET /blogs.json
  # ブログの一覧
  def index
    # 最近の公開エントリが多い順
    # JOIN 内で副問い合わせを行なう. (JOIN してから WHERE でフィルタするとエントリが得られない)
    # => Arel を組み合わせると失敗。混ぜると危険.
    
    join_cond = Entry.where('status <> 0 AND (date BETWEEN ? AND ?)',
                            Date.today - 90, Date.today)
    # left_outer_joins() はActiveRecord 5から.
    @blogs = Blog.joins('LEFT JOIN (' + join_cond.to_sql + ') AS recents ON blogs.id = recents.blog_id') \
                 .group('blogs.id, blogs.name, blogs.lead, blogs.urlkey') \
                 .select('DISTINCT blogs.id, blogs.name, blogs.lead, blogs.urlkey, COUNT(recents) AS cnt') \
                 .order('cnt DESC, blogs.id') \
                 .paginate(:page => params[:page], :per_page => 20)
  end


  # GET /blogs/1
  # GET /blogs/1.json
  # あるブログのトップ。リード文, 最新の記事などを表示.
  def show
    @entries = Entry.paginate(:page => params[:page], :per_page => 5) \
                    .where("blog_id = ? AND status <> 0 AND date <= ?", 
                                 @blog.id, Date.today) \
                    .order("date DESC, id DESC")

    @blog_tags = Tag.public_tags(@blog)
    @blog_members = @blog.members
  end


  # 1ヶ月分 (一般に公開)
  def month
    @mon = Date.new params[:year].to_i, params[:mon].to_i, 1
    @entries = Entry.where("blog_id = ? AND status <> 0 AND (date BETWEEN ? AND ?) AND date <= ?", @blog.id, @mon, (@mon >> 1) - 1, Date.today) \
                    .order("date, id")

    if @entries.empty?
      render :text => "not_found", :status => :not_found
      return
    end
    @blog_tags = Tag.public_tags(@blog)
    @blog_members = @blog.members
  end


  # GET /blogs/new
  def new
    @blog = Blog.new
  end


  # POST /blogs
  # POST /blogs.json
  def create
    @blog = Blog.new(blog_params)

    if @blog.save
      redirect_to @blog, notice: 'Blog was successfully created.'
    else
      render :new
    end
  end


  # GET /blogs/1/edit
  def edit
  end


  # PATCH/PUT /blogs/1
  # PATCH/PUT /blogs/1.json
  def update
    respond_to do |format|
      if @blog.update(blog_params)
        format.html { redirect_to @blog, notice: 'Blog was successfully updated.' }
        format.json { render :show, status: :ok, location: @blog }
      else
        format.html { render :edit }
        format.json { render json: @blog.errors, status: :unprocessable_entity }
      end
    end
  end


  # DELETE /blogs/1
  # DELETE /blogs/1.json
  def destroy
    @blog.destroy
    respond_to do |format|
      format.html { redirect_to blogs_url, notice: 'Blog was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


private ##################################################

  # Use callbacks to share common setup or constraints between actions.
  def set_blog
    urlkey = params[:blog_urlkey] || params[:urlkey]
    @blog = Blog.find_by_urlkey(urlkey)
    if !@blog
      render :text => "not_found: urlkey = #{urlkey}", :status => :not_found
    end
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def blog_params
    params.fetch(:blog, {})
  end

end # BlogsController
