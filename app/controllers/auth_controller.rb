# -*- coding:utf-8 -*-

# OmniAuth対応
class AuthController < ApplicationController
  # method POSTで投げてくるので, エラーにならないようにする
  protect_from_forgery :with => :null_session

  if Rails.env.production?
    PROVIDERS = ['facebook', 'google_oauth2']
  else
    PROVIDERS = ['developer', 'nov_op_sample', 'facebook', 'google_oauth2']
  end


  # 拒絶された場合
  #     env['omniauth.error'] = exception
  #     env['omniauth.error.type'] = message_key.to_sym
  #     env['omniauth.error.strategy'] = self
  def failure
    raise # TODO: impl.    
  end


  # /auth/:provider/callback
  # 承認された場合
  # env[key] ただし key is one of:
  #   "omniauth.strategy"
  #   "omniauth.origin"
  #   "omniauth.params"
  #   "omniauth.auth"
  def callback
    if !PROVIDERS.include?(params[:provider])
      raise "unknown provider: #{params[:provider]}"
    end

    case session["authmode"]
    when "signup"
      @user, @email = User.new_from_omniauth(env["omniauth.auth"])
      if Email.find_by_email_addr(@email.email_addr)
        flash[:alert] = "すでにユーザ登録されています。ログインしてください。"
        redirect_to :controller => "account", :action => "login"
        return
      end

      session[:email] = @email
      render "account/new"
      return

    when "login"
      user = User.authenticate_from_omniauth( env["omniauth.auth"] )
      if user
        set_current_user user, false
        request.session_options[:renew] = true

        redirect_back_or_default :controller => "users", :action => "show",
                                 :id => current_user
      else
        @error = "ログインに失敗: " + env["omniauth.auth"].inspect
        render "account/login"
        return
      end

    else
      @error = "ログインに失敗: unknown authmode: #{session["authmode"]}"
      render "account/login"
    end
  end


end # class AuthController
