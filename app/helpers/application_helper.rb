# -*- coding:utf-8 -*-


# controller, view の共通ルーチン
module ApplicationHelper
  # 文字列のUnicode正規化
  def normalize s, form = :kc
    raise TypeError if s && !s.is_a?(String)
    s ? s.mb_chars.normalize(form).to_s : nil
  end
  alias :normalize_kc :normalize


  # カレンダ付の日付選択
  def date_select2 obj, method
    r = date_select obj, method, {}, :style => "width:70px"
    r << %Q!<input type="button" onClick="openCalendar(this.form.#{obj}_#{method}_1i, this.form.#{obj}_#{method}_2i, this.form.#{obj}_#{method}_3i, event, &#39;yy/mm/dd&#39;);" value="..." style="padding:0">!.html_safe
    return r
  end


  # @raise [URI::InvalidURIError] uriとして不正な場合
  def norm_uri param
    raise TypeError if param && !param.is_a?(String)

    if param.to_s.strip != ""
      uri = URI.parse normalize_kc(param).strip
      uri.normalize!

      # "#!..."のときはママ
      if uri.fragment.to_s != "" && uri.fragment[0..0] != "!" 
        uri.fragment = ""
      end
      return uri
    else
      return nil
    end
  end


  # will_paginate() に与える.
  def blog_paginate_options
    return {:previous_label => '← Newer posts', :next_label => 'Older posts →'}
  end


  # 公開エントリかどうかでルーティングを変える
  def entry_path(entry)
    raise TypeError if !entry.is_a?(Entry)

    ret = {:controller => 'entries', :blog_urlkey => entry.blog.urlkey}
    
    if entry.status != 0 && entry.date <= Date.today
      return ret.merge( {:action => 'show', :id => entry.id} )
    else
      return ret.merge( {:action => 'preview', :entry_id => entry.id} )
    end
  end
end # module ApplicationHelper
