'use strict';

function replace_editor(textarea_id) 
{
  CKEDITOR.replace( textarea_id, {
    height: 360,
    //width: 560,
    toolbar: [
        // build-config.js で有効にしたものから選択.
        // 'Media Embed' は, extraPlugins: オプションで追加が必要。
        {name:'tools', items:['Templates', '-', 'Source', 'Maximize', 'ShowBlocks']}, // 'Preview'
        {name:'edit',
         items:['Cut', 'Copy', 'Paste', 'PasteText', '-', 'Undo', 'Redo', '-',
                'Replace']},
        {name:'insert',
         items:['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar']},
        {name:'links', items:['Link', 'Unlink']}, // 'Anchor'
        '/',
        {name:'paragraph',
         items:['NumberedList', 'BulletedList', 'Blockquote', 'Format']},
        {name:'basicstyles',
         items:['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', 'RemoveFormat']},
        {name:'styles', items:['TextColor', 'BGColor', 'Styles', 'FontSize']},
    ]} 
  );
}


var cnt = 1;

// ダイアログボックスで, このエントリのタグを追加。
function add_tag(name) 
{
    if (name == '')
        return;

    var exists = false;
    $('#tag_list *[name="tag[]"]').each( function(index) {
        if (name == $(this).val()) {
            exists = true;
            return false;
        }
    });
    if (exists) {
        $('#new_tag')[0].value = '';
        return;
    }
    
    var s = '<label class="secondary label" style="margin:0.5em">\
  <input type="hidden" name="tag[]" value="' + name + '" />' + name + '\
  <a style="background-color:white;font-size:12pt" onclick="remove_tag(this);return false;">\
    <span aria-hidden="true">&times;</span></a>\
</label>';

    $('#tag_list').append(s);
    cnt++;

    $('#new_tag')[0].value = '';
}


function add_new_tag()
{
    name = $('#new_tag')[0].value.trim();
    add_tag(name);

    // 候補にあったときは非表示
    $('#cand_tags a').each( function(index, elm) {
        if (name == elm.text) {
            $(elm).hide();
            return false;
        }
    });
}


// タグを削除する
function remove_tag(anchor) 
{
    name = $(anchor).parent().find('input')[0].value;
    
    var s = '<a class="small button" \
 onclick="add_tag(\'' + name + '\');$(this).hide();return false;">' +
            name + '</a>';

    // 候補に戻す
    $('#cand_tags a').each( function(index, elm) {
        if (name == elm.text) {
            $(elm).show();
            return false;
        }
    });

    //$('#cand_tags').append(s);
    $(anchor).parent().remove();
}


function hide_dup_tags()
{
    $('#cand_tags a').each( function(index, cand) {
        $('#tag_list input[name="tag[]"]').each( function(index, tag) {
            if (cand.text == $(tag).val())
                $(cand).hide();
        });
    });
}


/*
// エントリエディタに書き戻す
function update_tags() {
  $('#tag_list').empty();
  $('*[name=tag_name]').each( function(index) {
      s = '<input type="hidden" name="tag[]" value="' + $(this).text() + '" />' +
          '<button type="button" class="btn btn-default">' + $(this).text() + '</button>';
      $('#tag_list').append(s);
    } );
  $('#myModal').modal('hide');
}
*/
