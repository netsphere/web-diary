# -*- coding:utf-8 -*-


# ユーザグループ
class UserGroup < ActiveRecord::Base
  # 多対多
  has_many :users_user_groups
  has_many :users, :through => :users_user_groups

  # 所有
  has_many :blogs


  def member? user
    return false if !user
    
    if user.class.to_s == "Accounts::User"
      user = ::User.find_by_login(user.login)
    end
    if user.class.to_s != "User"
      raise TypeError, "user must be a User/Accounts::User, but #{user.class}" 
    end

    g = UsersUserGroup.where("user_id = ? AND user_group_id = ?",
                             user.id, self.id).first
    return (g ? true : false)
  end

end # class UserGroup
