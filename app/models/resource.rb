# -*- coding:utf-8 -*-

# Q's Web Diary
# Copyright (c) 2001,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 主題など
class Resource < ApplicationRecord

  # 多対多
  has_many :entries_resources
  has_many :entries, :through => :entries_resources

  URI_TYPES = [nil, "url", "asin"]

  class << self
    # @param item Nokogiri要素
    def amazon_book item
      raise TypeError if !item.is_a?(Nokogiri::XML::Element)
      
      attrs = item.css('ItemAttributes').first
      # <ItemLinks> 以下にレビューへのリンクがある。
      book = {
          :asin => item.css('ASIN').first.text, # PK
          :image_url => item.css('MediumImage URL').first &.text, # ないことがある
          :large_image => item.css('LargeImage URL').first &.text, # ないことがある
          :detail_link => item.css('DetailPageURL').first.text,

          # 共通
          :title => attrs.css('Title').first.text,
          :publisher => (attrs.css('Publisher').first ||
                       attrs.css('Manufacturer').first) &.text,  # Kindle版だとないことがある.
          :product_category => attrs.css('ProductGroup').first.text, # Book, DVD
          :product_type => attrs.css('ProductTypeName').first.text, # ABIS_BOOK, ABIS_VIDEO
          :EditorialReviews => item.css('EditorialReviews EditorialReview'),
          
          # Book
          :authors => (attrs.css('Author').map do |e| e.text end),
          :NumberOfPages => attrs.css('NumberOfPages').first &.text,
          :PublicationDate => attrs.css('PublicationDate').first &.text, # 2002-05 年月で固定か?

          # DVD
          :actors => (attrs.css('Actor').map do |e| e.text end),
          :directors => (attrs.css('Director').map do |e| e.text end), # 共同監督
          :IsAdultProduct => attrs.css('IsAdultProduct').first &.text, # 1
          :ReleaseDate => attrs.css('ReleaseDate').first &.text, # 2014-05-22
      }
      return book
    end
  end # class << self
  
end # class Resource
