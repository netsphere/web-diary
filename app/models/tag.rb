# -*- coding:utf-8 -*-

# Q's Web Diary
# Copyright (c) 2001,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# タグ
# タグそのものは、特定の blog ではなく, グローバルに持つ.
class Tag < ApplicationRecord
  # 多対多
  has_many :entries_tags
  has_many :entries, :through => :entries_tags

  belongs_to :created_by, :foreign_key => "create_user_id", 
                                                    :class_name => "User"

  validates_presence_of :name

  class << self
    def public_tags(blog)
      tags = Tag.joins('JOIN entries_tags ON tags.id = entries_tags.tag_id JOIN entries ON entries_tags.entry_id = entries.id') \
                .where('entries.status <> 0 AND entries.date <= ?', Date.today) \
                .group('tags.id, tags.name') \
                .select('tags.id, tags.name, COUNT(entries_tags) AS cnt')
      if blog
        return tags.where("blog_id = ?", blog.id)
      else
        return tags
      end
    end
  end # class << self
end # class Tag
