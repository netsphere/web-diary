# -*- coding:utf-8 -*-


# ユーザ.
# スタンドアロンでも動くようにする
class User < ApplicationRecord
  authenticates_with_sorcery!

  # 多対多
  has_many :users_user_groups
  has_many :user_groups, :through => :users_user_groups

  validates_uniqueness_of :login
  
  def blogs
    Blog.joins("LEFT JOIN user_groups ON blogs.user_group_id = user_groups.id " +
               "LEFT JOIN users_user_groups ON users_user_groups.user_group_id = user_groups.id") \
        .where("users_user_groups.user_id = ?", self.id)
  end


  def superuser
if Rails.env.production?
    return false
else
    return true # TODO: impl.
end
  end
end # class User
