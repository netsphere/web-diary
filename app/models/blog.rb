# -*- coding:utf-8 -*-

# Q's Web Diary
# Copyright (c) 2001,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# ブログ。他のテーブルはここにぶら下がる
class Blog < ApplicationRecord
  # 親
  belongs_to :user_group

  has_many :entries

  def member? user
    return false if !user
    if !user.is_a?(::User)
      user = ::User.find_by_login(user.login)
    end
    return false if !user

    b = Blog.joins("JOIN user_groups ON blogs.user_group_id = user_groups.id LEFT JOIN users_user_groups ON users_user_groups.user_group_id = user_groups.id") \
        .where("users_user_groups.user_id = ?", user.id)
    return b.size > 0
  end


  def members
    # 1段階, 間に挟まっている
    ug_id = user_group_id
    User.joins('JOIN users_user_groups ON users.id = users_user_groups.user_id') \
        .where('user_group_id = ?', ug_id) \
        .select('users.*')    
  end
  
end # class Blog
