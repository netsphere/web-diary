# -*- coding:utf-8 -*-


class UsersUserGroup < ActiveRecord::Base
  # 多対多
  belongs_to :user
  belongs_to :user_group
end
