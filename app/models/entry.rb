# -*- coding: utf-8 -*-

# Q's Web Diary
# Copyright (c) 2001,2011 HORIKAWA Hisashi. All rights reserved.
#   http://www.nslabs.jp/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# 日記一つ一つのエントリ (記事)
class Entry < ApplicationRecord

  # 親
  belongs_to :blog

  belongs_to :created_by, :foreign_key => "create_user_id", 
                                                    :class_name => "User"
  belongs_to :updated_by, :foreign_key => "update_user_id", 
                                                    :class_name => "User"

  has_many :attach_files

  has_many :comments

  # 多対多
  has_many :entries_tags
  has_many :tags, :through => :entries_tags

  # 多対多
  has_many :entries_resources
  has_many :resources, :through => :entries_resources

end # Entry

