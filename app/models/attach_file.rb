# -*- coding:utf-8 -*-

# 添付ファイル
class AttachFile < ActiveRecord::Base
  belongs_to :entry
  belongs_to :created_by, :foreign_key => "create_user_id", 
                                                    :class_name => "User"

end
