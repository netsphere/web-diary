# -*- coding:utf-8 -*-

module Accounts
  # スペース
  class UserGroup < Accounts::Base
    # 多対多
    has_many :user_groups_users
    has_many :users, :through => :user_groups_users
  end
end
