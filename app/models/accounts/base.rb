
if $ENABLE_SSO

module Accounts
  class Base < ActiveRecord::Base
    self.abstract_class = true
    establish_connection :accounts_db
  end
end

end # if $ENABLE_SSO
