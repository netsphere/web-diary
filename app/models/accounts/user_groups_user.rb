# -*- coding:utf-8 -*-

module Accounts

  class UserGroupsUser < Accounts::Base
    # 多対多
    belongs_to :user
    belongs_to :user_group
  end
end
