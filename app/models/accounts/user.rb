# -*- coding:utf-8 -*-

if $ENABLE_SSO

module Accounts
  # ユーザ
  class User < Accounts::Base
    # 多対多
    has_many :user_groups_users
    has_many :user_groups, :through => :user_groups_users
  end
end

end # if $ENABLE_SSO
