# -*- coding: utf-8 -*-

if $ENABLE_SSO

  
module Accounts

# "remember me"機能
class AuthToken < Accounts::Base
  #belongs_to :user, :class_name => "::Accounts::User"

  def user
    a_login = Accounts::Base.connection.select_rows("SELECT login FROM users WHERE id = #{user_id}").first
    return (a_login ? ::User.where("login = ?", a_login).first : nil)
  end

  
  def valid_remember_token?
    Time.now < expires_at 
  end
end
  
end # module Accounts

end # if $ENABLE_SSO
